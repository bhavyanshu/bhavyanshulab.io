---
layout: post
title: Istio Service Mesh - AWS EKS Loadbalancer configuration
date: 2019-09-30 22:11 -0500
---

There are few annotations in Kubernetes that can help you control how K8 modifies the EKS ELB configuration when you deploy the ingressgateway with Istio. Following annotations can be added to configure the ELB using YAML:

### Specify ACM Certificate

```yaml
apiVersion: v1
kind: Service
metadata:
  name: istio-ingressgateway
  namespace: istio-system
  annotations:
    service.beta.kubernetes.io/aws-load-balancer-ssl-cert: "arn:aws:acm:<code>"
```



### Enable SSL on multiple ports

```yaml
apiVersion: v1
kind: Service
metadata:
  name: istio-ingressgateway
  namespace: istio-system
  annotations:
    service.beta.kubernetes.io/aws-load-balancer-ssl-cert: "arn:aws:acm:<code>"
    service.beta.kubernetes.io/aws-load-balancer-ssl-ports: "443,15029,15030,15031"
```



### TLS termination at ELB

```yaml
service.beta.kubernetes.io/aws-load-balancer-backend-protocol: "http"
```



### Security Groups for ELB



```yaml
service.beta.kubernetes.io/aws-load-balancer-security-groups: "sg-zyx, sg-abc"
```



### IP addresses to restrict access

```yaml
spec:
  selector:
    app: istio-ingressgateway
    istio: ingressgateway
    release: istio
  sessionAffinity: None
  type: LoadBalancer
  loadBalancerSourceRanges: ["<ip>/32", "<ip>/32"]
```

### Reference

You can find more information here: https://kubernetes.io/docs/concepts/cluster-administration/cloud-providers/#aws

