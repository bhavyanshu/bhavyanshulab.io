---
layout: post
title: Certified Kubernetes Administrator (CKA) exam - few tips for you!
description: "Just sharing my thoughs about the CKA exam and providing some tips that I think will be useful."
tags: ["kubernetes", "certification"]
change_frequency: "weekly"
priority: 0.8
date: 2020-07-27 10:02 -0500
---

A little background about my experience with Kubernetes. I have been managing K8 clusters from past 2-3 years so I would say I am fairly
experienced in cluster management and troubleshooting. I decided to test my knowledge by taking the certification exam. The passing score 
is 74% and the exam duration is of 3 hours with about 24 questions to solve. I don't think time constraint was an issue as I was able to finish it in 2.5 hours. However, the grading seemed strict to me. I don't have an idea about how they actually grade. I was expecting a better score since I was able to complete all the tasks. In the end, I got 88% and [got certified](https://keybase.pub/bhavyanshu/files/bhavyanshu-parasher-certified-kubernetes-administrator-cka-certificate.pdf) but I am not sure what exactly I missed out on the tasks. So try to solve as much as you can on the test to score maximum.

Okay, so if you decide to take the test, it is important to practice a lot. Just memorizing the commands will not help you. These are some tips that I think will be helpful to you.

1. Familiarize yourself with the [official docs](https://kubernetes.io/docs/). You are allowed to refer to the official docs during the exam so knowing how to search and navigate the site is essential. It doesn't test you how good you are at memorizing stuff so that's the best thing. It wants you to learn the core concepts, read the examples and apply them just like how you would in real life.
2. It is important to focus on core concepts like, Pods, Deployments, Services, PersistentVolumes, PVCs, Roles, RoleBindings, ServiceAccounts, Networking (DNS mainly), and components of Kubernetes cluster (scheduler, proxy, kubelet, kubectl). You should know how to create and update these as required.
3. Practice in minikube. Setup your own minikube and practice a lot. Think about scenarios: For example, how would you run redis as a service
in a kubernetes cluster. How can the app service communicate with the redis service internally? Learn about [DNS of Pods and Services](https://kubernetes.io/docs/concepts/services-networking/dns-pod-service/).
4. Gain familiarity with Linux systemd services. You should be able to configure a systemd service and manage it (restart, status, etc). Majority of troubleshooting questions are around this. Learn how to use commands like "grep" and "sed". This will help you find strings and manipulate those quickly. Learn how to use vim. You do not need to learn everything about vim but just enough so you can edit and save the files.
5. Practice `kubectl run --generator=run-pod/v1 --dry-run -o yaml > example.yaml` to generate the yaml for a pod. This will make you more efficient
during the exam. This way you won't need to keep writing pod spec from scratch. Basically, practice all of `kubectl create` commands as they are really handy and use `--dry-run -o yaml` to generate yamls. This is extremely helpful.
6. Refer to the [cheatsheet](https://kubernetes.io/docs/reference/kubectl/cheatsheet/) if you blank out on basics or just need an example. It's okay if you can't remember. Like I said, they don't test you on how good you are at memorizing commands.
7. I don't think practice tests are required but you can find lot of courses that offer those. In my opinion, you should just practice in minikube and think of scenarios. As far as the cluster setup is concerned, read as much as you can about these topics: Kubeadm, openssl certs, CSR, kubelet and kubeproxy config, Static Pods, ectdctl (backup and restore). Basically, learn about any config that is used specified as part of `KUBELET_ARGS`

If you practice these topics, then you should be able to clear it. The exam is not very hard but understanding what the question is asking for is crucial to score better. Good luck! 