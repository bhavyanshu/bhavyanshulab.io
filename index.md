---
layout: page
title: Bhavyanshu Parasher
description : "Backend application developer, android application developer, python and php application developer."
tags: ["bhavyanshu parasher", "bhavyanshu", "android application developer","backend application developer"]
tagline:
change_frequency: "weekly"
priority: 1.0
---
{% include JB/setup %}
<div class="row">
 <div class="col-md-12 bio">
    <p>Currently working as a Systems Developer / DevOps Engineer with 6+ years of experience in software engineering. Feel free to check out my <a href="/projects">projects</a>.</p>
 </div>
</div>

<div class="row meta">
  <div class="col-md-4" id="contact">
    <h2>Contact</h2>
    <p><a href="https://drive.google.com/file/d/1ID8pdZWrYloSvHgAT7DmMB7RI02Ik2br/view?usp=drive_link" target="_blank">Resume</a></p>
    <p>Email me at <a href="mailto:mail@bhavyanshu.me">mail@bhavyanshu.me</a></p>
    <p>0xCD1967F4FDF1A9AB - Verify at <a target="_blank" href="https://keybase.io/bhavyanshu">keybase.io</a></p>
  </div>
  <div class="col-md-4 certs">
    <h2>
      Certifications
    </h2>
    <div class="wrap">
      <a href="https://keybase.pub/bhavyanshu/files/bhavyanshu-parasher-certified-kubernetes-administrator-cka-certificate.pdf" target="_blank">
        <img class="cert-img img-responsive" src="assets/imags/certs/cka.png" alt="CKA certified" />
      </a>
      <a href="https://bhavyanshu.keybase.pub/files/AWS%20Certified%20Solutions%20Architect%20-%20Associate%20certificate.pdf" target="_blank">
        <img class="cert-img img-responsive" src="assets/imags/certs/Logo_SAA_1176x600_Color.png" alt="AWS logo" />
        <img class="cert-img img-responsive" src="assets/imags/certs/Tag_SAA_588x300-Black.png" alt="AWS tag" />
      </a>
    </div>
  </div>
  <div class="col-md-4 social">
    <h2>
    Social
    </h2>
    <p class="social-icons">
      <a href="https://gitlab.com/{{ site.author.gitlab }}" target="_blank"><span class="fa fa-2x fa-gitlab"></span></a>
      <a href="https://www.linkedin.com/in/bhavyanshu/" target="_blank"><span class="fa fa-2x fa-linkedin"></span></a>
      <a href="https://twitter.com/bvynshu" target="_blank"><span class="fa fa-2x fa-twitter"></span></a>
      <a href="https://plus.google.com/112306240164215805986" rel="author" target="_blank"><span class="fa fa-2x fa-google-plus"></span></a>
      <a href="http://bhavyanshu.me/subscribe.html" target="_blank"><span class="fa fa-2x fa-rss"></span></a>
    </p>
    <p>You can visit <a href="/tags.html">tags section</a> to easily find topics of interest.</p>
  </div>
</div>
<hr/>
<div class="row">
  <div class="col-md-12 latest_posts">
    <h2>Posts</h2>
    <ul class="posts">
      {% for post in site.posts limit:20 %}
        <li><span>{{ post.date | date_to_string }}</span> &raquo; <a href="{{ BASE_PATH }}{{ post.url }}">{{ post.title }}</a></li>
      {% endfor %}
    </ul>
  </div>
</div>
